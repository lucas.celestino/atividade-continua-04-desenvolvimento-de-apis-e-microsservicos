from flask import Flask, jsonify, request
import requests

app = Flask(__name__)

# Rota GET para obter todos os TODOS da API
@app.route('/todos', methods=['GET'])
def get_todos():
    response = requests.get("https://jsonplaceholder.typicode.com/todos", verify=False)
    todos = response.json()
    return jsonify(todos)

# Rota POST para criar um novo TODO na API
@app.route('/todos', methods=['POST'])
def create_todo():
    todo = request.get_json()
    response = requests.post("https://jsonplaceholder.typicode.com/todos", json=todo, verify=False)
    new_todo = response.json()
    return jsonify(new_todo), 201

# Rota DELETE para excluir um TODO da API
@app.route('/todos/<int:todo_id>', methods=['DELETE'])
def delete_todo(todo_id):
    response = requests.delete(f"https://jsonplaceholder.typicode.com/todos/{todo_id}", verify=False)
    if response.status_code == 200:
        return jsonify({"message": f"Todo {todo_id} deleted successfully"})
    else:
        return jsonify({"error": "Failed to delete the Todo"}), 500

if __name__ == '__main__':
    app.run(port=5000)
