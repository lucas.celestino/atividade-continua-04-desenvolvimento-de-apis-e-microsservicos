from flask import Flask, jsonify, request
import requests
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://usuario:senha@localhost/nome_do_banco'
db = SQLAlchemy(app)

class Todo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255))
    completed = db.Column(db.Boolean)

@app.route('/todosBD', methods=['GET'])
def get_todos():
    todos = Todo.query.all()
    result = []
    for todo in todos:
        todo_data = {
            'id': todo.id,
            'title': todo.title,
            'completed': todo.completed
        }
        result.append(todo_data)
    return jsonify(result)

# Função para fazer uma requisição GET ao primeiro serviço
def get_todos():
    response = requests.get("http://localhost:5000/todos")
    todos = response.json()
    return todos

# Função para fazer uma requisição POST ao primeiro serviço
def create_todo(todo):
    response = requests.post("http://localhost:5000/todos", json=todo)
    new_todo = response.json()
    return new_todo

# Função para fazer uma requisição DELETE ao primeiro serviço
def delete_todo(todo_id):
    response = requests.delete(f"http://localhost:5000/todos/{todo_id}")
    if response.status_code == 200:
        return True
    else:
        return False

# Rota GET para obter todos os usando o primeiro serviço
@app.route('/client/todos', methods=['GET'])
def get_todos_client():
    todos = get_todos()
    return jsonify(todos)

# Rota POST para criar um novo usando o primeiro serviço
@app.route('/client/todos', methods=['POST'])
def create_todo_client():
    todo = request.get_json()
    new_todo = create_todo(todo)
    return jsonify(new_todo), 201

# Rota DELETE para excluir um usando o primeiro serviço
@app.route('/client/todos/<int:todo_id>', methods=['DELETE'])
def delete_todo_client(todo_id):
    deleted = delete_todo(todo_id)
    if deleted:
        return jsonify({"message": f"Todo {todo_id} deleted successfully"})
    else:
        return jsonify({"error": "Failed to delete the Todo"}), 500

if __name__ == '__main__':
    app.run(port=5001)