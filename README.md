# API de Todos

Esta é uma API simples para gerenciar uma lista de tarefas (todos). A API possui as seguintes rotas:

## Rotas

### GET /todos
Retorna a lista de todos os todos.

### POST /todos
Cria um novo todo.

### DELETE /todos/{todo_id}
Exclui um todo específico com o ID fornecido.

## Uso

Para utilizar a API, siga as etapas abaixo:

1. Faça uma requisição GET para `/todos` para obter a lista de todos os todos.
2. Para criar um novo todo, faça uma requisição POST para `/todos` com os dados do todo no corpo da requisição.
3. Para excluir um todo específico, faça uma requisição DELETE para `/todos/{todo_id}`, onde `{todo_id}` é o ID do todo a ser excluído.

Certifique-se de fornecer os dados corretos no formato adequado ao criar um novo todo ou excluir um todo existente.

Exemplo de corpo da requisição para criação de um todo:

